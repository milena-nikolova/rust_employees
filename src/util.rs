use chrono::prelude::*;
use std::collections::{BTreeSet, HashMap};
use std::env::args;
use std::fs::File;
use std::io::{self, BufRead, Write};
use std::path::Path;

use crate::model::*;

pub fn get_file_location() -> String {
    args().nth(1).unwrap_or_else(|| get_file_location_console())
}

fn get_file_location_console() -> String {
    let mut file_location = String::new();
    println!("Specify input file:");
    let _ = io::stdout().flush();
    io::stdin()
        .read_line(&mut file_location)
        .expect("Failed to read file name");

    file_location.trim().to_string()
}

pub fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where
    P: AsRef<Path>,
{
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}

pub fn get_duration_dates(
    first_assignment: &Assignment,
    second_assignment: &Assignment,
) -> Assignment {
    let start_date = if first_assignment.get_start_date() > second_assignment.get_start_date() {
        first_assignment.get_start_date()
    } else {
        second_assignment.get_start_date()
    };
    let end_date = if first_assignment.get_end_date() < second_assignment.get_end_date() {
        first_assignment.get_end_date()
    } else {
        second_assignment.get_end_date()
    };
    Assignment::new(*start_date, *end_date)
}

pub fn has_overlap(first_assignment: &Assignment, second_assignment: &Assignment) -> bool {
    first_assignment.get_start_date() <= second_assignment.get_end_date()
        && second_assignment.get_start_date() <= first_assignment.get_end_date()
}

pub fn calc_duration(assignment: &Assignment) -> i64 {
    assignment
        .get_end_date()
        .signed_duration_since(*assignment.get_start_date())
        .num_days()
        + 1 // add last day
}

pub fn read_employees(
    lines: std::io::Lines<std::io::BufReader<std::fs::File>>,
) -> HashMap<u32, AssignmentMap> {
    const DATE_FORMAT: &str = "%Y-%m-%d";
    let mut employees = HashMap::new();

    for line in lines {
        if let Ok(record) = line {
            let parts: Vec<&str> = record.split(',').map(|s| s.trim()).collect();

            let emp_id: u32 = match parts[0].parse() {
                Ok(num) => num,
                Err(_) => {
                    println!("Invalid employee Id. Skipping row..");
                    continue;
                }
            };
            let project_id: u32 = match parts[1].parse() {
                Ok(num) => num,
                Err(_) => {
                    println!("Invalid project Id. Skipping row..");
                    continue;
                }
            };
            let start_date: NaiveDate = match NaiveDate::parse_from_str(&parts[2], DATE_FORMAT) {
                Ok(date) => date,
                Err(_) => {
                    println!("Invalid start date. Skipping row..");
                    continue;
                }
            };
            let start_date: AssignmentDate = Date::from_utc(start_date, Utc);
            let end_date: AssignmentDate = if parts[3] == "NULL" {
                Utc::now().date()
            } else {
                let end_date = match NaiveDate::parse_from_str(&parts[3], DATE_FORMAT) {
                    Ok(date) => date,
                    Err(_) => {
                        println!("Invalid end date. Skipping row..");
                        continue;
                    }
                };
                Date::from_utc(end_date, Utc)
            };

            employees
                .entry(emp_id)
                .or_insert_with(|| AssignmentMap::new())
                .entry(project_id)
                .or_insert_with(|| Vec::new())
                .push(Assignment::new(start_date, end_date));
        }
    }
    employees
}

pub fn generate_employee_pairs(employees: HashMap<u32, AssignmentMap>) -> BTreeSet<EmployeePair> {
    let mut employee_pairs = BTreeSet::new();
    let emp_keys: Vec<u32> = employees.keys().cloned().collect();
    let keys: Vec<(&u32, &u32)> = emp_keys
        .iter()
        .enumerate()
        .flat_map(|(i, a)| emp_keys[i + 1..].iter().map(move |b| (a, b)))
        .collect();

    for (a, b) in keys {
        let mut pair = EmployeePair::new(*a, *b);
        pair.calc_days_working_together(&employees[a], &employees[b]);
        employee_pairs.insert(pair);
    }

    employee_pairs
}

pub fn print_employee_pairs(employee_pairs: BTreeSet<EmployeePair>) {
    let mut max_days = 0;
    for pair in employee_pairs.iter().rev() {
        if max_days > pair.get_days_working_together() {
            break;
        }
        max_days = pair.get_days_working_together();
        println!("Pair with max days: {}", pair);
    }
}
