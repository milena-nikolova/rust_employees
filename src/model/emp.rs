use chrono::prelude::*;
use std::collections::{HashMap, HashSet};
use std::fmt;

pub type AssignmentMap = HashMap<u32, Vec<Assignment>>;
pub type AssignmentDate = Date<Utc>;

use crate::util::*;

#[derive(Eq, PartialEq, Hash)]
pub struct Assignment {
    start_date: AssignmentDate,
    end_date: AssignmentDate,
}

impl Assignment {
    pub fn new(start_date: AssignmentDate, end_date: AssignmentDate) -> Self {
        Self {
            start_date,
            end_date,
        }
    }

    pub fn get_start_date(&self) -> &AssignmentDate {
        &self.start_date
    }

    pub fn get_end_date(&self) -> &AssignmentDate {
        &self.end_date
    }
}

#[derive(Eq, PartialEq, Ord, PartialOrd)]
pub struct EmployeePair {
    days_working_together: i64,
    first_employee_id: u32,
    second_employee_id: u32,
}

impl EmployeePair {
    pub fn new(first_employee_id: u32, second_employee_id: u32) -> Self {
        Self {
            first_employee_id,
            second_employee_id,
            days_working_together: 0,
        }
    }

    pub fn get_first_employee_id(&self) -> u32 {
        self.first_employee_id
    }

    pub fn get_second_employee_id(&self) -> u32 {
        self.second_employee_id
    }

    pub fn get_days_working_together(&self) -> i64 {
        self.days_working_together
    }

    pub fn calc_days_working_together(
        &mut self,
        first_emp_assignments: &AssignmentMap,
        second_emp_assignments: &AssignmentMap,
    ) {
        let first_projects = first_emp_assignments.keys().collect::<HashSet<_>>();
        let second_projects = second_emp_assignments.keys().collect::<HashSet<_>>();
        let common_projects = first_projects
            .intersection(&second_projects)
            .collect::<Vec<_>>();

        let mut common_project_dates: Vec<Assignment> = Vec::new();
        let mut overlaps: HashSet<Assignment> = HashSet::new();

        for pr in common_projects {
            let first_assignments: &Vec<Assignment> = first_emp_assignments.get(pr).unwrap();
            let second_assignments: &Vec<Assignment> = second_emp_assignments.get(pr).unwrap();

            for first_assignment in first_assignments {
                for second_assignment in second_assignments {
                    if has_overlap(&first_assignment, &second_assignment) {
                        let common = get_duration_dates(&first_assignment, &second_assignment);
                        common_project_dates.push(common);
                    }
                }
            }
        }

        for dates in &common_project_dates {
            self.days_working_together += calc_duration(&dates);
            let current_overlaps: HashSet<Assignment> = common_project_dates
                .iter()
                .filter(|overlap| *overlap != dates && has_overlap(overlap, &dates))
                .map(|overlap| get_duration_dates(overlap, dates))
                .collect();
            overlaps.extend(current_overlaps);
        }

        for overlap in overlaps {
            self.days_working_together -= calc_duration(&overlap);
        }
    }
}

impl fmt::Display for EmployeePair {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "First Employee: {}, Second Employee {}, Days working together: {}",
            self.get_first_employee_id(),
            self.get_second_employee_id(),
            self.get_days_working_together()
        )
    }
}
