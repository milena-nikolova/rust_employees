use std::io::{self};

mod model;
mod util;

use crate::util::*;

fn main() -> io::Result<()> {
    let file = get_file_location();
    if let Ok(lines) = read_lines(&file) {
        let employees = read_employees(lines);
        let employee_pairs = generate_employee_pairs(employees);

        print_employee_pairs(employee_pairs);
    } else {
        println!("File not found: {}", file);
    }

    Ok(())
}
